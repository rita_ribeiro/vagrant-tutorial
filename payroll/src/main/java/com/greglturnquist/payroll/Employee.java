/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String emailField;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, String emailField) {
        validateParams(firstName, lastName, description, jobTitle, emailField);
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.jobTitle = jobTitle;
        this.emailField = emailField;
    }

    private void validateParams(String firstName, String lastName, String description, String jobTitle, String emailField) {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateDescription(description);
        validateJobTitle(jobTitle);
        validateEmailFields(emailField);
    }

    private void validateFirstName(String firstName){
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Invalid firstName");
        }
    }

    private void validateLastName(String lastName){
        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Invalid lastName");
        }
    }

    private void validateDescription(String description){
        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("Invalid description");
        }
    }

    private void validateJobTitle(String jobTitle){
        if (jobTitle == null || jobTitle.isEmpty()) {
            throw new IllegalArgumentException("Invalid jobTitle");
        }
    }

    private void validateEmailFields(String emailField){
        if (emailField == null || emailField.isEmpty()) {
            throw new IllegalArgumentException("Invalid emailField");
        }
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(emailField);

        if (!matcher.matches()){
            throw new IllegalArgumentException("Invalid email");
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(emailField, employee.emailField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, description, jobTitle, emailField);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        validateFirstName(firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        validateLastName(lastName);
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        validateDescription(description);
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        validateJobTitle(jobTitle);
        this.jobTitle = jobTitle;
    }

    public String getEmailField() {
        return emailField;
    }

    public void setEmailField(String emailField) {
        validateEmailFields(emailField);
        this.emailField = emailField;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", emailField='" + emailField + '\'' +
                '}';
    }
}
// end::code[]
