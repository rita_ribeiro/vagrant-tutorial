package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Return true when two objects are equal - same object")
    void testEqualsSameObject() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //act
        boolean result = employee.equals(employee);
        //assert
        assertTrue(result);
    }

    @Test
    @DisplayName("Return true when two objects are equal - different object")
    void testEqualsDifferentObject() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        Employee employee1 = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertTrue(result);
    }

    @Test
    @DisplayName("Return false when two objects are different")
    void testNotEqualsDifferentObject() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        Employee employee1 = new Employee("Rita1", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different because one of them is null")
    void testNotEqualNull() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //act
        boolean result = employee.equals(null);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return true when hashcode's are equal - same object")
    public void testHashEqualsSameObject() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //assert
        assertTrue(employee.hashCode() == employee.hashCode());
    }

    @Test
    @DisplayName("Return true when hashcode's are equal - different object")
    public void testHashEqualsDifferentObjects() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        Employee employee1 = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //assert
        assertTrue(employee.hashCode() == employee1.hashCode());
    }

    @Test
    @DisplayName("Return false when hashcode's are different")
    public void testHashDifferent() {
        //arrange
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        Employee employee1 = new Employee("Rita1", "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //assert
        assertTrue(employee.hashCode() != employee1.hashCode());
    }

    @Test
    @DisplayName("Public constructor - throws Exception with first name null")
    void firstNameNullThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee(null, "Ribeiro", "description", "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with first name empty")
    void firstNameEmptyThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("", "Ribeiro", "description", "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Ensure i can get the first name")
    void getFirstName() {
        //arrange
        String expected = "Rita";
        Employee employee = new Employee(expected, "Ribeiro", "description", "engineer", "1191746@isep.ipp.pt");
        //act
        String result = employee.getFirstName();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the first name for the expected value - throws exception because is null")
    void setFirstNameNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setFirstName(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the first name for the expected value - throws exception because is empty")
    void setFirstNameEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setFirstName("");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with last name null")
    void lastNameNullThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", null, "description", "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with last Name empty")
    void lastNameEmptyThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "", "description", "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Ensure i can get the last name")
    void getLastName() {
        //arrange
        String expected = "Ribeiro";
        Employee employee = new Employee("Rita", expected, "description", "engineer", "1191746@isep.ipp.pt");
        //act
        String result = employee.getLastName();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the last name for the expected value - throws exception because is null")
    void setLastNameNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setLastName(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the last name for the expected value - throws exception because is empty")
    void setLastNameEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setLastName("");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with description null")
    void descriptionNullThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", null, "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with description empty")
    void descriptionEmptyThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("", "Ribeiro", "", "engineer", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Ensure i can get the description")
    void getDescription() {
        //arrange
        String expected = "description";
        Employee employee = new Employee("Rita", "Ribeiro", expected, "engineer", "1191746@isep.ipp.pt");
        //act
        String result = employee.getDescription();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the description for the expected value - throws exception because is null")
    void setDescriptionNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setDescription(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the description for the expected value - throws exception because is empty")
    void setDescriptionEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setDescription("");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with job title null")
    void jobTitleNullThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", null, "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with job title empty")
    void jobTitleEmptyThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", "", "rita@isep.ipp.pt");
        });
    }

    @Test
    @DisplayName("Ensure i can get the job title")
    void getJobTitle() {
        //arrange
        String expected = "engineer";
        Employee employee = new Employee(expected, "Ribeiro", "description", expected, "1191746@isep.ipp.pt");
        //act
        String result = employee.getJobTitle();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the job title for the expected value - throws exception because is null")
    void setJobTitleNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setJobTitle(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the job title for the expected value - throws exception because is empty")
    void setJobTitleEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setJobTitle("");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with email field null")
    void emailFieldNullThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", "engineer", null);
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with email field empty")
    void emailFieldEmptyThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", "engineer", "");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with email with no @")
    void emailThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", "engineer", "email");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with email with no dot")
    void emailThrowsExceptionConstructorNoDot() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Rita", "Ribeiro", "description", "engineer", "email@testcom");
        });
    }

    @Test
    @DisplayName("Ensure i can get the email field")
    void getEmailField() {
        //arrange
        String expected = "1191746@isep.ipp.pt";
        Employee employee = new Employee("Rita", "Ribeiro", "description", "engineer", expected);
        //act
        String result = employee.getEmailField();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the email field - throws exception because is null")
    void setEmailFieldNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmailField(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the email field - throws exception because is empty")
    void setEmailFieldEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmailField("");
        });
    }

    @Test
    @DisplayName("Ensure i can not set the email - throws exception because has not the @")
    void setEmailField() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmailField("email");
        });
    }

    @Test
    @DisplayName("Ensure i can not set the email - throws exception because has not the dot")
    void setEmailFieldWithoutDot() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmailField("email@testcom");
        });
    }
}